package cn.mingtech.deeplinkdemo;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends Activity {

    WebView web_main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        web_main = findViewById(R.id.web_main);
        web_main.loadUrl("file:///android_asset/h5.html");
        web_main.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (url.startsWith("will://")) {
                    Uri uri = Uri.parse(url);
                    Log.e("---------scheme: ", uri.getScheme() + "host: " + uri.getHost() + "Id: " + uri.getPathSegments().get(0));
                    Toast.makeText(MainActivity.this, "打开新的页面", Toast.LENGTH_LONG).show();
                    return true; //返回true，代表要拦截这个url
                }

                return super.shouldOverrideUrlLoading(view, url);

            }
        });
    }
}
